set number
set mouse=a
set numberwidth=1
set clipboard=unnamed
syntax enable
set showcmd
set ruler
set encoding=utf-8
set showmatch
set sw=4 
set relativenumber
set laststatus=2
set noshowmode
" set bg=dark

call  plug#begin('~/.vim/plugged')

" Temas
Plug 'morhetz/gruvbox'
Plug 'sickill/vim-monokai'
Plug 'flazz/vim-colorschemes'
    

" IDE
Plug 'easymotion/vim-easymotion'
Plug 'scrooloose/nerdtree'
Plug 'christoomey/vim-tmux-navigator'


" Sintax

Plug 'jelera/vim-javascript-syntax'
Plug 'mxw/vim-jsx'
Plug 'elzr/vim-json'
Plug 'mattn/emmet-vim'

" React

Plug 'Shougo/deoplete.nvim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'w0rp/ale'

" Definiciones JS
Plug 'carlitux/deoplete-ternjs'
Plug 'ternjs/tern_for_vim'

call plug#end()

" colorscheme typewriter
" colorscheme typewriter-night
" color oscuro
" colorscheme gruvbox
colorscheme molokai

let g:gruvbox_contrast="hard"
let NERDTreeQuitOnOpen=1

let mapleader = " "

nmap <Leader>s <Plug>(easymotion-s2)
nmap <Leader>nt :NERDTreeFind<CR>

nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>

let g:deoplete#enable_at_startup = 1
